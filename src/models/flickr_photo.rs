use std::time::SystemTime;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::models::photo::Photo;

#[derive(Debug, Clone, Serialize, Deserialize, Hash)]
pub struct FlickrPhoto {
    pub id: String,
    pub owner: String,
    pub secret: String,
    pub server: String,
    pub farm: u8,
    pub title: String,
    pub ispublic: u8,
    pub isfriend: u8,
    pub isfamily: u8,
    pub originalsecret: String,
    pub originalformat: String,
    #[serde(default = "default_date")]
    pub harvest_datetime: DateTime<Utc>,
    #[serde(default)]
    pub downloaded: bool,
}

fn default_date() -> DateTime<Utc> {
    DateTime::from(SystemTime::now())
}

impl PartialEq for FlickrPhoto {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
            && self.secret == other.secret
            && self.farm == other.farm
            && self.server == other.server
    }
}

impl Eq for FlickrPhoto {}

impl Photo for FlickrPhoto {}
