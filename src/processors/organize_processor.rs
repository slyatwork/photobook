use std::path::PathBuf;

use async_trait::async_trait;
use chrono::Datelike;

use crate::models::picture::Picture;
use crate::processors::processor::Processor;
use crate::processors::report::Report;
use crate::services::filesystem_reader::FileSystemReader;
use crate::services::filesystem_writer::FileSystemWriter;

pub(crate) struct OrganizeProcessor {
    from: PathBuf,
    to: PathBuf,
    filesystem_reader: FileSystemReader,
}

impl OrganizeProcessor {
    pub fn new(from: PathBuf, to: PathBuf) -> Self {
        OrganizeProcessor {
            from,
            to,
            filesystem_reader: FileSystemReader::new(),
        }
    }

    fn organize(&self, harvest_report: &mut Report) {
        let files = self.filesystem_reader.list_files_in_dir(self.from.as_path());
        let pictures = self.filesystem_reader.list_pictures_in_files(files);
        harvest_report.files_to_process = pictures.len() as u32;

        for picture in pictures {
            let (year, month, day): (String, String, String) = self.get_photo_taken_date(&picture);
            let dest_dir = match FileSystemWriter::check_dir_exists_or_create(self.to.clone(), year, month, day) {
                Some(path) => path,
                None => self.to.clone().into_os_string().into_string().unwrap_or_default()
            };
            let dest_path = [dest_dir, picture.filename].join("/");
            match FileSystemWriter::copy_picture(picture.path, dest_path) {
                Ok(_) => harvest_report.files_processed += 1,
                Err(_) => harvest_report.files_aborted += 1,
            }
        }
    }

    fn get_photo_taken_date(&self, picture: &Picture) -> (String, String, String) {
        let taken_date = match picture.picture_datetime {
            Some(date) => date,
            None => picture.file_datetime
        };
        let year = taken_date.year().to_string();
        let month = OrganizeProcessor::add_0_to_short_numbers(taken_date.month().to_string());
        let day = OrganizeProcessor::add_0_to_short_numbers(taken_date.day().to_string());
        (year, month, day)
    }

    fn add_0_to_short_numbers(number: String) -> String {
        if number.len() < 2 {
            "0".to_owned() + &number
        } else {
            number
        }
    }
}

#[async_trait]
impl Processor for OrganizeProcessor {
    async fn process(&self) -> Result<Report, Vec<String>> {
        let mut harvest_report: Report = Report::new();
        let mut errors: Vec<String> = Vec::new();

        if !self.from.exists() || !self.from.is_dir() {
            harvest_report.errors += 1;
            errors.push("from path given does not exists or is not a directory".into());
        }
        if !self.to.exists() || !self.to.is_dir() {
            harvest_report.errors += 1;
            errors.push("target path given does not exists or is not a directory".into());
        }

        self.organize(&mut harvest_report);

        if harvest_report.errors > 0 {
            Err(errors)
        } else {
            Ok(harvest_report)
        }
    }
}
