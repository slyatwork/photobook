use async_trait::async_trait;

use crate::processors::report::Report;

#[async_trait]
pub(crate) trait Processor {
    async fn process(&self) -> Result<Report, Vec<String>>;
}
