#[derive(Debug, Clone)]
pub(crate) struct Report {
    pub files_to_process: u32,
    pub files_processed: u32,
    pub files_aborted: u32,
    pub files_deleted: u32,
    pub errors: u32,
}

impl Report {
    pub fn new() -> Self {
        Report {
            files_to_process: 0,
            files_processed: 0,
            files_aborted: 0,
            files_deleted: 0,
            errors: 0
        }
    }
}
