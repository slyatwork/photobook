use async_trait::async_trait;

use crate::processors::report::Report;
use crate::services::es_client::ElasticClient;

#[async_trait]
pub(crate) trait Harvester {
    async fn initialize_db(&self) -> Result<Report, Vec<String>> {
        let _clean_result = self.get_es_client().delete_all_records().await;
        self.update_db().await
    }

    async fn update_db(&self) -> Result<Report, Vec<String>>;

    async fn clean_db(&self) -> Result<Report, Vec<String>>;

    fn get_es_client(&self) -> &ElasticClient;

}
