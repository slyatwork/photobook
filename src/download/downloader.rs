use std::collections::HashSet;

use async_trait::async_trait;

use crate::errors::*;
use crate::models::photo::Photo;
use crate::processors::report::Report;

#[async_trait]
pub(crate) trait Downloader<T> where T: Photo + Sync + Send {
    async fn download_photos(&self, photos: HashSet<T>) -> Result<Report>;
}
