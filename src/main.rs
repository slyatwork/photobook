//! # PhotoBook
//!
//! `PhotoBook` is a command line tool to harvest, organize, download from clouds and backup your photos.
//! `PhotoBook`is packaged with an Elasticsearch database and Kibana as consultation interface.
//!
//! Simply install it from cargo to try it !
//! ```
//! cargo install photobook
//! ```
//!
//! For further information and usage please refer to its repository : https://gitlab.com/slyatwork/photobook
//!


extern crate confy;
#[macro_use]
extern crate error_chain;
extern crate reqwest;

use chrono::Utc;
use structopt::StructOpt;

use crate::cli::Cli;
use crate::errors::*;
use crate::processors::download_processor::DownloadProcessor;
use crate::processors::harvest_processor::HarvestProcessor;
use crate::processors::organize_processor::OrganizeProcessor;
use crate::processors::processor::Processor;
use crate::services::es_client::ElasticClient;

pub mod constants;

pub mod cli;
pub mod config;

pub mod models {
    pub mod picture;
    pub mod flickr_photo;
    pub mod photo;
}

pub mod helpers { pub mod es_helper; }

pub mod harvest {
    pub mod filesystem_harvester;
    pub mod flickr_harvester;
    pub mod harvester;
}

pub mod download {
    pub mod downloader;
    pub mod flickr_downloader;
}

pub mod services {
    pub mod es_client;
    pub mod filesystem_reader;
    pub mod filesystem_writer;
    pub mod flickr_client;
}

pub mod processors {
    pub mod processor;
    pub mod harvest_processor;
    pub mod download_processor;
    pub mod organize_processor;
    pub mod report;
}

pub mod errors {
    error_chain! {
        foreign_links {
            Io(std::io::Error);
            HttpRequest(reqwest::Error);
            Config(confy::ConfyError);
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    println!("{} - CLI process started", Utc::now().format("%Y-%m-%d %H:%M:%S"));

    let args = Cli::from_args();
    println!("Process launched for {:?}", &args);

    match args {
        Cli::Harvest { source, operation, path } => {
            if is_elastic_running().await {
                let processor = HarvestProcessor::new(source, operation, path);
                match processor.process().await {
                    Ok(n) => println!("harvest report: {:?}", n),
                    Err(n) => println!("errors occurred: {:?}", n)
                };
            } else {
                print_elastic_warning();
            }
        }
        Cli::Organize { from, to } => {
            let processor = OrganizeProcessor::new(from, to);
            match processor.process().await {
                Ok(n) => println!("organize report: {:?}", n),
                Err(n) => println!("errors occurred: {:?}", n)
            };
        }
        Cli::Download { source, to, server } => {
            if is_elastic_running().await {
                let processor = DownloadProcessor::new(source, to, server);
                match processor.process().await {
                    Ok(n) => println!("download report: {:?}", n),
                    Err(n) => println!("errors occurred: {:?}", n)
                };
            } else {
                print_elastic_warning();
            }
        }
        Cli::Backup { from, to } => println!("{:?} - {:?}\nSorry, Organize process is not implemented at this time", from, to),
    }
    println!("{} - CLI process ended", Utc::now().format("%Y-%m-%d %H:%M:%S"));

    Ok(())
}

async fn is_elastic_running() -> bool {
    let client = ElasticClient::new("");
    match client.get_indices().await {
        Ok(indices) => indices.len() > 0,
        Err(_) => false
    }
}

fn print_elastic_warning() {
    println!("It seems elasticsearch is not running yet as the operation chosen will need it !\n\
        - if you use a distant server, please verify its accessibility and your config\n\
        - if you use a local docker stack, check the dockers are well launched\n  \
          *remember* type 'docker-compose down && docker-compose up -d' where the docker-compose.yml provided lands.")
}
