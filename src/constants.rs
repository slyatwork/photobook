pub const ELASTIC_URL: &str = "http://localhost:9200";
pub const ELASTIC_PICTURES_INDEX: &str = "pictures";
pub const ELASTIC_FLICKR_PHOTOS_INDEX: &str = "flickr_photos";

pub const FLICKR_API_URL: &str = "https://api.flickr.com/services/rest";
pub const FLICKR_NONCE: &str = "comtelabs";
