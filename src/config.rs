use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Config {
    pub flickr_key: String,
    pub flickr_secret: String,
    pub flickr_oauth_token: String,
    pub flickr_oauth_secret: String,
}
