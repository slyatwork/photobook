use std::fs::{copy, create_dir_all};
use std::path::{Path, PathBuf};

pub(crate) struct FileSystemWriter {}

impl FileSystemWriter {
    pub fn check_dir_exists_or_create(to: PathBuf, year: String, month: String, day: String) -> Option<String> {
        let to_as_str = to.as_os_str().to_str().unwrap_or_default();
        let path = [to_as_str, year.as_str(), month.as_str(), day.as_str()].join("/");

        if Path::new(path.as_str()).exists() {
            Some(path)
        } else {
            if Path::new(to_as_str).exists() {
                let result = create_dir_all(&path.as_str());
                if result.is_ok() {
                    return Some(path)
                }
            }
            None
        }
    }

    pub fn copy_picture(from: String, to: String) -> Result<u64, String>{
        match copy(from, to) {
            Ok(i) => Ok(i),
            Err(_) => Err("error during process".into())
        }
    }
}
